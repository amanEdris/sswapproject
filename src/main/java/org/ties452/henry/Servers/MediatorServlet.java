package org.ties452.henry.Servers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.apache.commons.io.FileUtils;
//import org.apache.commons.io.LineIterator;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.FileManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

/**
 * Servlet implementation class ServletTest
 */
@WebServlet(name = "MediatorServlet", urlPatterns = {"/path"})
public class MediatorServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private String RDGfilePath = "/Users/edris/NetBeansProjects/SswapCottage/src/main/java/org/ties452/henry/RDGFiles/rdg.rdf";
    private String RIGfilePath = "/Users/edris/NetBeansProjects/SswapCottage/src/main/java/org/ties452/henry/RDGFiles/rig.rdf";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MediatorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String jspPage = request.getParameter("jspPage");

        startRunning();

        if (jspPage.equals("index") ) {
            //@todo make get request to remote service 

            //get the file from the InputStream
            File file = new File(RIGfilePath);
            //get the subject inputs
            ArrayList<Object> arr = GetSubjectElements(file);

            String result = this.renderToView(arr);
            request.setAttribute("javascript", result);
            RequestDispatcher rd = request.getRequestDispatcher("/test.jsp");
            rd.forward(request, response);
        } else if (jspPage.equals("test")) {
            ArrayList<Object> inputList = new ArrayList<Object>();
            inputList.add(request.getParameter("bookerFirstName"));
            inputList.add(request.getParameter("cityOfInterest"));
            inputList.add(request.getParameter("distanceToCityOfInterest"));
            inputList.add(request.getParameter("maximumShift"));
            inputList.add(request.getParameter("requiredAmountOfDays"));
            inputList.add(request.getParameter("requiredBedrooms"));
            inputList.add(request.getParameter("requiredDistanceToLake"));
            inputList.add(request.getParameter("requiredOccupacy"));
            inputList.add(request.getParameter("startingBookingDay"));
            System.out.println(inputList.toString());
        }
    }

    /**
     * controller Method
     */
    public void startRunning() {

        InputStream in = FileManager.get().open(RDGfilePath);
        if (in == null) {
            throw new IllegalArgumentException("File: " + RDGfilePath
                    + " not found");
        } else {
            System.out.println("file actually existed! \n");
        }
        // read file
        Model m2 = ModelFactory.createDefaultModel();
        m2.read(in, null, "TTL");


        FileWriter out = null;
        try {
            out = new FileWriter(RIGfilePath);
            m2.write(out, "TURTLE");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



    }

    /**
     * Get the subject Elements
     */
    public ArrayList<Object> GetSubjectElements(File RIGfilePath) {
        LineIterator it;

        ArrayList<Object> arr = new ArrayList<Object>();
        try {
            it = FileUtils.lineIterator(RIGfilePath, "UTF-8");
            while (it.hasNext()) {
                String line = it.nextLine();
                if (line.contains("[ a       sswap:Subject ,")) {
                    line = it.nextLine();
                    do {

                        if (line.contains(":")) {
                            trimLine(line, arr);
                        }/*
                         * else { //if the line does not begin with the ":"
                         * character String[] word = line.trim().split("\\s+");
                         * String input = word[0]; arr.add(input); }
                         */
                        line = it.nextLine();
                    } while (!line.contains("sswap:mapsTo"));
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return arr;
    }

    /**
     * Trim off the prefix and empty characters
     *
     * @param line
     *
     */
    private void trimLine(String line, ArrayList<Object> arr) {
        String[] word = line.trim().split("\\:");
        String[] word0 = word[1].split("\\s+");
        arr.add(word0[0]);

    }

    /**
     * renders the browser view
     */
    public String renderToView(ArrayList<Object> arr) {
        String result = null;
        result = " ";
        for (int i = 0; i < arr.size(); i++) {
            result += "$('#label" + (i + 1) + "').html('" + arr.get(i) + "');";
        }
        return result;

    }

    /**
     * custom ontology Alignment
     *
     * @return returns a boolean value
     */
    private boolean AlignOntology() {

        return false;
    }
}
